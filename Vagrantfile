# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

  #cache server "apache traffic server"
  config.vm.define "ats" do |ats|
    ats.vm.box = "ubuntu64"
    ats.vm.network "private_network", ip: "192.168.10.10"

    ats.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "512"]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
    end

    ats.vm.provision "ansible" do |ansible|
      ansible.playbook="ats-cache.yml"
      ansible.inventory_path="development"
      ansible.host_key_checking=false
      ansible.limit='all'
    end
  end

  #apt-cache server
  config.vm.define "squid" do |squid_deb_cache|
    squid_deb_cache.vm.box = "ubuntu64"
    squid_deb_cache.vm.network "private_network", ip: "192.168.10.2"

    squid_deb_cache.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "512"]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
    end

    squid_deb_cache.vm.provision "ansible" do |ansible|
      ansible.playbook="squid-deb-cache.yml"
      ansible.inventory_path="development"
      ansible.host_key_checking=false
      ansible.limit='all'
    end
  end

  #nginx load balancer instance
  config.vm.define "nginx" do |nginx|
    nginx.vm.box = "ubuntu64"
    nginx.vm.network "private_network", ip: "192.168.10.3"

    nginx.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "512"]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
    end

    nginx.vm.provision "ansible" do |ansible|
      ansible.playbook="nginx.yml"
      ansible.inventory_path="development"
      ansible.host_key_checking=false
      ansible.limit='all'
    end
  end

  #website instance 1
  config.vm.define "web1" do |web1|
    web1.vm.box = "ubuntu64"
    web1.vm.network "private_network", ip: "192.168.10.4"

    web1.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "512"]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
    end

    web1.vm.provision "ansible" do |ansible|
      ansible.playbook="webserver.yml"
      ansible.inventory_path="development"
      ansible.host_key_checking=false
      ansible.limit='all'
    end
  end

  #postgres database instance
  config.vm.define "db" do |db|
    db.vm.box = "ubuntu64"
    db.vm.network "private_network", ip: "192.168.10.5"

    db.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", "512"]
      vb.customize ["modifyvm", :id, "--cpus", "1"]
    end

    db.vm.provision "ansible" do |ansible|
      ansible.playbook="pg.yml"
      ansible.inventory_path="development"
      ansible.host_key_checking=false
      ansible.limit='all'
    end
  end
end
